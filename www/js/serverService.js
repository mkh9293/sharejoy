app.service('serverService',function($http){
	function httpError(data,status,headers,config){
		alert('server error' + status);
		w = window.open();
		w.document.write(data);
	}

	var SERVER_URL = 'http://192.168.0.7:8080/shareJoy';

	function httpPost(url,params,callback){
		$http({
			method:'post',
			url:SERVER_URL + url,
			params:params
		})
		.success(function(data,status,headers,config){callback(data);})
		.error(httpError);
	}
	function httpGet(url,params,callback){
		$http({
			method:'get',
			url:SERVER_URL + url,
			params:params
		})
		.success(function(data,status,headers,config){callback(data);})
		.error(httpError);
	}
	// 파일 업로드 기능 추가중.
	// create할 때 이미지도 같이 서버에 올려야함.
	function fileUpload(url,fileKey,fileUrl,callback){
		function uploadError(error){
			alert('ErrCode:'+error.code+" ErrSource"+error.source+" ErrTarget:"+error.target);
		}
		function uploadSuccess(data){
			callback(JSON.parse(data.response));
		}
		var options = new FileUploadOptions();
		options.fileKey = fileKey;
		options.fileName = fileUrl; 
		/*fileUrl.subString(fileUrl.lastIndexOf('/')+1);*/
		options.mimeType = "image/jpeg";
		/*options.params = params;*/
		var fileTransfer = new FileTransfer();
		fileTransfer.upload(fileUrl,encodeURI(SERVER_URL+url),uploadSuccess,uploadError,options);
	}
	this.login = function(loginId,passwd,callback){
		httpPost('/member/login',{loginId:loginId, password:passwd},callback);
	}

	this.signup = function(member,callback){
		httpPost('/member/signup',member,callback);
	}

	this.create = function(board,callback){
		httpPost('/board/create',board,callback);
	}

	this.loadView = function(id,callback){
		httpGet('/board/view',{id: id},callback);
	}

	this.loadViewList = function(count,callback){
		httpGet('/board/viewList',{count:count},callback);
	};

	this.delete = function(id,userId,callback){
		httpGet('/board/delete',{id:id,userId:userId},callback);
	};

	this.imageUpload = function(imageURL,callback){
		fileUpload('/image/imageUpload',"file",imageURL,callback);
	};

	this.imageDownload = function(imageId){
		return SERVER_URL+'/image/'+imageId+'/imageDownload';
	};	

	this.loadNewsList = function(searchNews,callback){
		httpPost('/news/newsList',{searchNews:searchNews},callback);
	}
	this.imageDelete = function(imageId,callback){
		httpGet('/image/'+imageId+'/imageDelete',null,callback);
	}
});