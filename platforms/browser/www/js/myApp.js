var app = angular.module('myApp', ['ngRoute', 'mobile-angular-ui', 'mobile-angular-ui.gestures']);

app.run(function ($transform) {
    window.$transform = $transform;
});

app.config(['$routeProvider',function myRouteConfig($routeProvider) {
    $routeProvider.
    when('/',{templateUrl: 'home.html',reloadOnSearch: false,controller:'HomeController'}).
    when('/today',{templateUrl:'today.html',reloadOnSearch:false,controller:'TodayController'}).
    when('/login', {templateUrl: 'login.html',reloadOnSearch: false,controller: 'LoginController'}).
    when('/news', {templateUrl: 'news.html',reloadOnSearch: false,controller: 'NewsController'}).
    when('/viewNews/:index', {templateUrl: 'viewNews.html',reloadOnSearch: false,controller: 'ViewNewsController'}).
    when('/signup', {templateUrl: 'signup.html',reloadOnSearch: false,controller: 'SignupController'}).
    when('/mypage',{templateUrl: 'mypage.html',reloadOnSearch: false,controller:'ViewNewsController'}).
    when('/create',{templateUrl:'create.html',reloadOnSearch: false,controller: 'CreateController'}).
    when('/view/:index',{templateUrl:'view.html',reloadOnSearch:false,controller:'ViewController'}).
    when('/postView/:index',{templateUrl:'view.html',reloadOnSearch:false,controller:'PostViewController'}).
    when('/viewEdit/:index',{templateUrl:'viewEdit.html',reloadOnSearch:false,controller:'ViewEditController'});
}]);

app.controller('MainController', function ($rootScope, $scope,$location,$sce,serverService) {
    $scope.viewList = [];
    $scope.titleList = [];
    $scope.newsList = [];
    $scope.activeTab = 1;

    $scope.loadViewList = function(count){
        serverService.loadViewList($scope.titleList.length + count,function(data){
            $scope.titleList = data;
        });
    };

    $scope.loadView = function(index){
        if(!$scope.viewList[index]){
            serverService.loadView(index,function(data){
                data.body = $sce.trustAsHtml(data.body);
                $scope.viewList[index] = data;
            });
        }
    };
    $scope.loadViewList(10);

    $scope.loadNewsList = function(){
        // 처음에는 검색조건 ""로 설정
        serverService.loadNewsList("",function(data){
            $scope.newsList = data;
        });
    };

    $scope.loadNewsList();


    $scope.logout = function(){
        $rootScope.member = null;
        $scope.activeTab = 1;
        $location.path("/");
    };

    $scope.gotoEdit = function(){
        $location.path('/viewEdit/'+$rootScope.index);
    };

    // Needed for the loading screen
    $rootScope.$on('$routeChangeStart', function() {
        $rootScope.loading = true;
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $rootScope.loading = false;
    });
});
app.controller('LoginController', function ($rootScope,$scope,$location,serverService) {
    if($rootScope.member){
        $location.path("/mypage");
    }
    $scope.login = function () {

        serverService.login($scope.memberId,$scope.password,function(data){
            if(data){
                 $rootScope.member = data;
                 $location.path("/mypage");
             }else {
                 $rootScope.member = null;
                 alert("아이디나 비밀번호가 잘못되었습니다.");
             }
        });
    };

});
app.controller('SignupController', function ($scope,$location,serverService) {
    $scope.pwError=0;
    $scope.pwErrorCheck = 0;
    $scope.idError=0;
    $scope.gender = '남';

    $scope.idBlur = function(userId){ 
        if(userId === undefined){
            $scope.idErrMsg = '3~10글자의 영문과 숫자만 사용 가능합니다.';
            $scope.idError=1;
        } 
        else $scope.idError=0;
    };


    $scope.pwBlur = function(userPass){
        if(userPass === undefined){
            $scope.pwErrMsg = '6자 이상 15자이하로 작성해주세요.';
            $scope.pwError = 1;
        }
        else $scope.pwError = 0;
    };
    
    $scope.pwBlurCheck = function(userPass){
        if(userPass === undefined) $scope.pwErrorCheck = 1;
        else $scope.pwErrorCheck = 0;
    };
   
    
    $scope.selectGender = function (i) {
        if (i == '남') $scope.gender = '남';
        else $scope.gender = '여';
    };
   
    $scope.signup = function () {
        var member = {
            memberId: $scope.userId,
            password: $scope.password,
            passwordCheck: $scope.passwordCheck,
            gender: $scope.gender
        };

        serverService.signup(member,function(data){
            alert(data.msg);
            $location.path('/login');
        });

    };
   
});
app.controller('ViewController', function ($rootScope,$scope,$location,$routeParams,serverService) {
    var index = $routeParams.index;
    $rootScope.index = index;
    $scope.loadView(index);

    $scope.delete = function(){
        serverService.delete(index,$rootScope.member.member_id,function(data){
            if(data.success){
                $scope.loadViewList(0);
                $location.path('/');
            }else{
                alert(data.message);
            }
        });
    };

});
app.controller('PostViewController', function ($rootScope,$scope,$location,$routeParams,$sce,serverService) {
    var index = $routeParams.index;
    $rootScope.index = index;
    serverService.loadView(index,function(data){
        data.body = $sce.trustAsHtml(data.body);
        $scope.viewList[index] = data;
    });

    $scope.delete = function(){
        serverService.delete(index,$rootScope.member.member_id,function(data){
            if(data.success){
                $scope.loadViewList(0);
                $location.path('/');
            }else{
                alert(data.message);
            }
        });
    };
});
app.controller('CreateController',function ($rootScope, $compile,$scope,$location,$anchorScroll,serverService,photoService) {
    if(!$rootScope.member){
        $location.path("/login");
    };
    var bodySize = 0;
    var imgs = [];

    $scope.create = function(){
        var bodyText = document.getElementById("createBody");
        imgs = [];
        var board = {
            member_id:$rootScope.member.member_id,
            boardType:0,
            title:$scope.title,
            body:bodyText.innerHTML
            /*$scope.body.replace(/\n/g,"<br>")*/
        };

        serverService.create(board,function(data){
            if(data.success){
                alert(data.message);
                $scope.loadViewList(1);
                $location.path('/postView/'+data.index);
            }else{
                alert(data.message);
            }
        });
    };

    /*$scope.resizeTextArea = function(el) {
       if(el.keyCode==13){
        var bodyH =  document.getElementById('postText');
        bodyH.style.height = "1px";
        bodyH.style.height = (20 + bodyH.scrollHeight) + "px";
       }
    };*/

    $scope.returnTextArea = function() {
        $location.hash('ancMiddle');
        $anchorScroll();
        $location.hash('ancTop');
        $anchorScroll();
    };

    function onSuccess(imageURL){
        serverService.imageUpload(imageURL,function(data){
            if(data.message!=null){
                imgs.push(data.message);
                var url = serverService.imageDownload(data.message);
                var oDoc = document.getElementById("createBody");
                var temp = '<div contenteditable="false" id="img'+data.message+'"><a ng-click="imageDelete('+data.message+')"></a><p><img src="'+url+'"></p></div><br/>';
                var tempDoc = $compile(temp)($scope); 
                angular.element(document.getElementById('createBody')).append(tempDoc);
                /*angular.element(document.getElementById('createBody')).innerHTML+= tempDoc;*/
            }else
                alert('사진 업로드 실패');
        });
    }

    $scope.fromAlbum = function(){
        photoService.fromAlbum(onSuccess);
    }

    //나중에 트랜잭션 처리 해야함.
    $scope.imageDelete = function(id){
        serverService.imageDelete(id,function(data){
            if(data.success){
                alert(data.message);

                //삭제된 이미지를 imgs배열에서 삭제
                var ind = imgs.indexOf(id);
                imgs.splice(ind,1);

                //타겟 div 제거 javascript만 이용.
                var parentDiv = document.getElementById("createBody");
                var targetDiv = document.getElementById("img"+id);
                parentDiv.removeChild(targetDiv);

                // 다른 방법임. jquery를 사용한 방법.
                // angular.element(document.getElementById('img'+id)).remove();   
            }else
                alert(data.message);
        });
        
    }

    $scope.cancel = function(){
        if(imgs.length == 0){
            $location.path('/');
        }else{
            serverService.imageDelete(imgs,function(data){
                if(data.success){
                   alert(data.message);
                   $location.path('/');
                }else
                   alert(data.message);
            });
        }
    }
   
});

app.controller('ViewEditController', function ($rootScope,$sce,$scope,$compile,$location,$routeParams,serverService,photoService) {
    var index = $routeParams.index;
    $scope.title = $scope.viewList[index].title;

    // $scope.viewList[index].body = $compile($scope.viewList[index].body)($scope);
    var imgs = [];
    /*var body = $scope.viewList[index].body+"";
    $scope.body = body.replace(/<br>/g,'\n');*/

    $scope.update = function(){
        imgs = [];
        var bodyText = document.getElementById("viewEditBody");
        var board = {
            board_id:index,
            boardType:1,
            member_id:$rootScope.member.member_id,
            title:$scope.title,
            body:bodyText.innerHTML
            /*$scope.body.replace(/\n/g,"<br>")*/
        };

        serverService.create(board,function(data){
            if(data.success){
                $scope.loadViewList(0);
                $location.path('/postView/'+data.index);
            }else{
                alert(data.message);
            }
        });
    };

    $scope.fromAlbum = function(){
        photoService.fromAlbum(onSuccess);
    }

    function onSuccess(imageURL){
        serverService.imageUpload(imageURL,function(data){
            if(data.message!=null){
                imgs.push(data.message);
                var url = serverService.imageDownload(data.message);
                var oDoc = document.getElementById("viewEditBody");
                var temp = '<div contenteditable="false" id="img'+data.message+'"><a ng-click="imageDelete('+data.message+')"></a><p><img src="'+url+'"></p></div><br/>';
                var tempDoc = $compile(temp)($scope); 
                angular.element(document.getElementById('viewEditBody')).append(tempDoc);
            }else
                alert('사진 업로드 실패');
        });
    }

    $scope.imageDelete = function(id){
        serverService.imageDelete(id,function(data){
             alert(data.message);

            //삭제된 이미지를 imgs배열에서 삭제
            var ind = imgs.indexOf(id);
            imgs.splice(ind,1);

            //타겟 div 제거
            var parentDiv = document.getElementById("viewEditBody");
            var targetDiv = document.getElementById("img"+id);
            parentDiv.removeChild(targetDiv);
        });
    }

    $scope.cancel = function(){
        if(imgs.length == 0){
            $location.path('/');
        }else{
            serverService.imageDelete(imgs,function(data){
                if(data.success){
                    alert("성공");
                    $location.path('/');
                }else
                    alert("실패");
            });
        }
    }

    $scope.fromAlbum = function(){
        photoService.fromAlbum(onSuccess);
    }

});
app.controller('HomeController', function ($rootScope, $scope,$location) {
    if($rootScope.home_scrollTop) {
         setTimeout(restoreScrollPosition,300);
    }
    function saveScrollPosition(){
        $rootScope.home_scrollTop = $("div#home_list").scrollTop();
    }
    function restoreScrollPosition(){
        $("div#home_list").scrollTop($rootScope.home_scrollTop);
    }

    $scope.gotoView = function(index){
        saveScrollPosition();
        $location.path('/view/'+index);
    };

});
app.controller('NewsController', function ($rootScope,$scope,$location,serverService) {
  
    $scope.searchNewsList = function(){
        var searchNews = $scope.searchNews;
        // 검색조건 추가
        serverService.loadNewsList(searchNews,function(data){
            $scope.$parent.newsList = data;
        });
    }

    if($rootScope.home_scrollTop) {
         setTimeout(restoreScrollPosition,300);
    }
    function saveScrollPosition(){
        $rootScope.home_scrollTop = $("div#home_list").scrollTop();
    }
    function restoreScrollPosition(){
        $("div#home_list").scrollTop($rootScope.home_scrollTop);
    }

    $scope.gotoNews = function(index){
        saveScrollPosition();
        $location.path('/viewNews/'+index);
    }
});
app.controller('ViewNewsController', function ($rootScope,$scope,$routeParams,serverService) {
    var index = $routeParams.index;
    $rootScope.index = index;
    $scope.news = $scope.newsList[index];
});

app.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}])
