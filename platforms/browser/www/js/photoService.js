app.service('photoService',function(){
	function onError(message){
		alert('Error: '+message);
	}

	this.fromAlbum = function(callback){
		var options = {
			destinationType:navigator.camera.DestinationType.FILE_URL,
			sourceType:navigator.camera.PictureSourceType.SAVEDPHOTOALBUM
		}
		navigator.camera.getPicture(callback,onError,options);
	}
});